# Des ouvrages pour compléter et qui ont nourri cette présentation
## Les réseaux de zéro
Vincent Sénétaire, Jean-Manassé Pouabou, Titouan Soulard  
Eyrolles, Paris, 2022, 372 p.

## Les réseaux, l'ère des réseaux cloud et de la 5G
Guy Pujolle  
Eyrolles, Paris, 2018, 1087 p.

## Les secrets de l'image vidéo, 12e édition
Philippe Bellaïche  
Eyrolles, Paris, 2021, 818 p.

## Les RFCs
https://www.ietf.org/standards/rfcs/  
https://www.rfc-editor.org/