.PHONY: all clean

CC := pandoc
CFLAGS := -t revealjs -s

MEXT := md
SRC := $(sort $(wildcard *.$(MEXT)))
#DEST := $(SRC:.md=.html)
METADATA := metadata.yaml
TARGET := presentation.html

all: ${TARGET}

$(TARGET): $(SRC) $(METADATA)
	$(CC) $(CFLAGS) $(SRC) --metadata-file=$(METADATA) -o $@

clean:
	rm -rf $(TARGET)
