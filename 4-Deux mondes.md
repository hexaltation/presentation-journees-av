## IT / AV un choc des cultures
### IT
- Méthodologie de projet à temps longs,
- Culture de la documentation,
- Volonté de ne pas bouger de qui marche,
- Car modifier risque de tout casser.

### AV
- Besoin évenementiel,
- Parfois pour demain, voire pour hier,
- Une tendance intuitive, presque intuitionniste des outils de travail (peut-être en corrélation avec la dimmension créative ou artistique).

### Comment on se parle
- Des réponses dans la conférence de cet après-midi.
